# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/fortinetdev/fortios" {
  version     = "1.13.2"
  constraints = "1.13.2"
  hashes = [
    "h1:OwBua4Q6kJ0dUXc2NkI/3tkLfRrgeTv0xzzNLg0ohDI=",
    "zh:3d790c8b799cb2a202bc9925c32d7a8124311a605a47c79d9987dd979f691573",
    "zh:4fdc3c66fc27af124bc9f3e282fd23183cb471b122602907a6813c7fcc2614ea",
    "zh:6cb0fd203ef9df4fd2b005819d74bd583f9085113f02ec944b33cc6829c81a36",
    "zh:8a0b941a16ae4bb406eda8a2666855bfc9406912d4cb83c1a3c3e42d0fb9f123",
    "zh:8ba38702ee0fcfbd4a9907ed129168402eee18ec238eebb52cf49461eeea0545",
    "zh:a910b814f6b5e497e9f1228e9a6e88c941197226be40a36e34d88d96c22d1d57",
    "zh:af8bd1fcafeb458276af18e7f1599a5e99eea56f01766394e47caebde3c6dde2",
    "zh:bb6baa3f84b3189b87edfff92e015d5ee2f7af675196c5eba28cb81f8728683b",
    "zh:bfc9477fe6ebbf29514ddd5da67d034f30cf8db22860453e1be6822ef15d319b",
    "zh:da4740fa575e0cdccb756c7deef4b5532773fdf44d16722543865c441caf97c3",
    "zh:db890fbef85089e3bf69eb97ef5e0dc5b31f502b6fab636c1d236f56a1acc13a",
    "zh:f6100819ec367ee68dd3acf2b7b82385cb9f7330cf1bc1472a0905b77f1493ef",
  ]
}
