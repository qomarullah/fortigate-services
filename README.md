## Please update files in main.tf

```sh
resource "fortios_firewallservice_custom" "esb_service" {
  app_service_type    = "disable"
  category            = "General"
  check_reset_range   = "default"
  color               = 0
  helper              = "auto"
  iprange             = "0.0.0.0"
  name                = "esb_service"
  protocol            = "TCP/UDP/SCTP"
  protocol_number     = 6
  proxy               = "disable"
  tcp_halfclose_timer = 0
  tcp_halfopen_timer  = 0
  tcp_portrange       = "7000-8000"
  tcp_timewait_timer  = 0
  udp_idle_timer      = 0
  visibility          = "enable"
}
```